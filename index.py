import sys
import win32gui
import time

def callback(hwnd, strings):
    if win32gui.IsWindowVisible(hwnd):
        window_title = win32gui.GetWindowText(hwnd)
        left, top, right, bottom = win32gui.GetWindowRect(hwnd)
        if window_title and right-left and bottom-top:
            strings.append(window_title)
    return True

log_file = open("C:\\Users\\liz\\Documents\\Git\\filmscantimer\\scan_log.csv", "a")

def log(msg):
    log_file.write(msg + "\n")

def done():
    log_file.close()
    sys.exit(0)

def main():
    state = False
    launchTime = time.time()
    window = 120.0 # It has two minutes to launch
    while not state:
        win_list = []
        win32gui.EnumWindows(callback, win_list)
        if win_list.count("CyberViewX5") > 0:
            state = True
        else:
            time.sleep(0.5)
            if time.time() - launchTime > window:
                done()
    state = False
    while True:
        win_list = []  # list of strings containing win handles and window titles
        win32gui.EnumWindows(callback, win_list)  # populate list
        if win_list.count("CyberViewX5") > 1:
            if not state:
                print("START," + str(time.time()))
                state = True
        elif win_list.count("CyberViewX5") == 1:
            if state:
                print("END," + str(time.time()))
                state = False
        else:
            # CyberViewX5 closed
            print("CyberViewX5 Not Running")
            done()
        time.sleep(0.01)

    done()


if __name__ == '__main__':
    main()